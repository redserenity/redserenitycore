<?php

namespace {

	class MyService implements \RedSerenity\Core\Service {

		protected $Random = NULL;

		public function __construct() {

			$this->Random = "Hello #" . rand(1000,10000);

		}

		public function SayHello() {
			echo $this->Random . PHP_EOL;
		}

	}

	class Something extends \RedSerenity\Core\Injection {

		public function __construct() {

			$this->MyService->SayHello();

		}

		public function __inject(MyService $MyService) {}

	}


	echo "The following two numbers should be different." . PHP_EOL;
	$Something = new Something();
	$SomethingAgain = new Something();

}