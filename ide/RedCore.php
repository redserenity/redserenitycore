<?php

namespace RedSerenity {

    class Exception extends \Exception {}

}

namespace RedSerenity\Core {

    interface Injectable {
        public function __inject();
    }

    interface Service {}

    class Exception extends \RedSerenity\Exception {}

    class SharedService implements Service {

        protected $__INSTANCE__;

        protected function __construct();
        private function __clone();
        private function __wakeup();
        static public function __instance();
    }

}