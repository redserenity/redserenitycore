/* $Id$ */

#ifndef PHP_REDCORE_H
#define PHP_REDCORE_H

extern zend_module_entry RedCore_module_entry;
#define phpext_redcore_ptr &RedCore_module_entry

#ifdef PHP_WIN32
#	define PHP_REDCORE_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define PHP_REDCORE_API __attribute__ ((visibility("default")))
#else
#	define PHP_REDCORE_API
#endif

#ifdef ZTS
#include "TSRM.h"
#endif


ZEND_BEGIN_MODULE_GLOBALS(RedCore)
	zend_bool DisableRedCore;
	zend_bool DisableInjectable;
	zend_bool DisableEvents;
	zend_bool DisableSingleton;

	zend_bool CaptureEventsOutput;

	zend_bool DebugRedCore;

	HashTable htEvents;
	HashTable htObjects;
ZEND_END_MODULE_GLOBALS(RedCore)

#define REDCORE_GLOBAL(v) ZEND_MODULE_GLOBALS_ACCESSOR(RedCore, v)


#define IS_REDCORE_DISABLED() do { \
	if (REDCORE_GLOBAL(DisableRedCore)) { \
		zend_error(E_WARNING, "RedCore Extension has been disabled."); \
		return FAILURE; \
	} \
} while(0)

#define IS_REDCORE_INJECTABLE_DISABLED() do { \
	if (REDCORE_GLOBAL(DisableInjectable)) { \
		zend_error(E_WARNING, "RedCore Injectable has been disabled."); \
		return FAILURE; \
	} \
} while(0)

#define IS_REDCORE_EVENTS_DISABLED() do { \
	if (REDCORE_GLOBAL(DisableEvents)) { \
		zend_error(E_WARNING, "RedCore Events have been disabled."); \
		return FAILURE; \
	} \
} while(0)

#define IS_REDCORE_SINGLETON_DISABLED() do { \
	if (REDCORE_GLOBAL(DisableSingleton)) { \
		zend_error(E_WARNING, "RedCore Singleton has been disabled."); \
		return FAILURE; \
	} \
} while(0)


#if defined(ZTS) && defined(COMPILE_DL_REDCORE)
ZEND_TSRMLS_CACHE_EXTERN()
#endif

#endif	/* PHP_REDCORE_H */


/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
