/* $Id$ */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"
#include "php_redcore.h"
#include "redcore.h"
#include "src/dummy/dummy.h"
#include "src/exception/base.h"
#include "src/exception/core.h"
#include "src/injectable/injectable.h"
#include "src/service/service.h"
#include "src/singleton/singleton.h"
#include "src/events/events.h"


ZEND_DECLARE_MODULE_GLOBALS(RedCore)

/* True global resources - no need for thread safety here */
static int le_redcore;


PHP_INI_BEGIN()
	STD_PHP_INI_ENTRY("redcore.disable", "0", PHP_INI_ALL, OnUpdateBool, DisableRedCore, zend_RedCore_globals, RedCore_globals)
	STD_PHP_INI_ENTRY("redcore.disable_injectable", "0", PHP_INI_ALL, OnUpdateBool, DisableInjectable, zend_RedCore_globals, RedCore_globals)
	STD_PHP_INI_ENTRY("redcore.disable_events", "0", PHP_INI_ALL, OnUpdateBool, DisableEvents, zend_RedCore_globals, RedCore_globals)
	STD_PHP_INI_ENTRY("redcore.disable_singleton", "1", PHP_INI_ALL, OnUpdateBool, DisableSingleton, zend_RedCore_globals, RedCore_globals)

	STD_PHP_INI_ENTRY("redcore.capture_events_output", "0", PHP_INI_ALL, OnUpdateBool, CaptureEventsOutput, zend_RedCore_globals, RedCore_globals)
	STD_PHP_INI_ENTRY("redcore.debug", "0", PHP_INI_ALL, OnUpdateBool, DebugRedCore, zend_RedCore_globals, RedCore_globals)
PHP_INI_END()

static void php_RedCore_init_globals(zend_RedCore_globals *RedCoreGlobals) {
	memset(RedCoreGlobals, 0, sizeof(zend_RedCore_globals));
}


PHP_MINIT_FUNCTION(RedCore) {
	REGISTER_INI_ENTRIES();

	if (REDCORE_GLOBAL(DisableRedCore)) {
		return SUCCESS;
	}

	MINIT_ExceptionBase();
	MINIT_ExceptionCore();


	if (!REDCORE_GLOBAL(DisableInjectable)) {
		MINIT_InjectableInterface();
		MINIT_ServiceClassInterface();
	}

	if (!REDCORE_GLOBAL(DisableEvents)) {
		MINIT_EventHooks();
	}

	if (!REDCORE_GLOBAL(DisableSingleton)) {
		MINIT_SingletonInterface();
	}

	return SUCCESS;
}
PHP_MSHUTDOWN_FUNCTION(RedCore) {
	UNREGISTER_INI_ENTRIES();

	if (!REDCORE_GLOBAL(DisableEvents)) {
		MSHUTDOWN_EventHooks();
	}

	return SUCCESS;
}

PHP_RINIT_FUNCTION(RedCore) {
	#if defined(COMPILE_DL_REDCORE) && defined(ZTS)
	ZEND_TSRMLS_CACHE_UPDATE();
	#endif

	if (REDCORE_GLOBAL(DisableRedCore)) {
		return SUCCESS;
	}

	if (!REDCORE_GLOBAL(DisableEvents)) {
		zend_hash_init(&REDCORE_GLOBAL(htEvents), 8, NULL, ZVAL_PTR_DTOR, 0);
		zend_hash_init(&REDCORE_GLOBAL(htObjects), 8, NULL, ZVAL_PTR_DTOR, 0);
	}

	return SUCCESS;
}

PHP_RSHUTDOWN_FUNCTION(RedCore) {

	if (REDCORE_GLOBAL(DisableRedCore)) {
		return SUCCESS;
	}

	if (!REDCORE_GLOBAL(DisableEvents)) {
		zend_hash_destroy(&REDCORE_GLOBAL(htEvents));
		zend_hash_destroy(&REDCORE_GLOBAL(htObjects));
	}

	return SUCCESS;
}

PHP_MINFO_FUNCTION(RedCore) {
	php_info_print_box_start(0);
	php_printf("%s", PHP_MODULE_DESCRIPTION);
	php_info_print_box_end();

	php_info_print_table_start();
	php_info_print_table_header(2, "RedSerenity Core (RedCore)", "enabled");
	php_info_print_table_row(2, "Author", PHP_MODULE_AUTHOR);
	php_info_print_table_row(2, "Website", PHP_MODULE_WEBSITE);
	php_info_print_table_row(2, "Version", PHP_MODULE_VERSION);
	php_info_print_table_row(2, "Build Date", __DATE__ " " __TIME__ );
	php_info_print_table_end();

	DISPLAY_INI_ENTRIES();
}

zend_module_entry RedCore_module_entry = {
	STANDARD_MODULE_HEADER,	PHP_MODULE_NAME,
	RedCoreFunctions,
	PHP_MINIT(RedCore),	PHP_MSHUTDOWN(RedCore),
	PHP_RINIT(RedCore),	PHP_RSHUTDOWN(RedCore),
	PHP_MINFO(RedCore),	PHP_MODULE_VERSION,
	STANDARD_MODULE_PROPERTIES
};

#ifdef COMPILE_DL_REDCORE
#ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE()
#endif
ZEND_GET_MODULE(RedCore)
#endif
