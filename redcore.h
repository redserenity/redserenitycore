#ifndef REDCORE_H
#define REDCORE_H

#include "php.h"
#include "php_redcore.h"

#define PHP_MODULE_NAME "RedCore"
#define PHP_MODULE_PROJECT "RedSerenity"
#define PHP_MODULE_WEBSITE "http://www.redserenity.com"
#define PHP_MODULE_AUTHOR "Tyler Andersen <tyler@redserenity.com>"
#define PHP_MODULE_VERSION "0.5.0"
#define PHP_MODULE_DESCRIPTION "RedSerenity Core provides the following features: Base Exceptions for the RedSerenity Platform, Injectable Services, and true Method-level Events."
#define NAMESPACE_ROOT "RedSerenity"
#define NAMESPACE_CORE "RedSerenity\\Core"
#define NAMESPACE_EVENTS "RedSerenity\\Core\\Events"

ZEND_EXTERN_MODULE_GLOBALS(RedCore)
#define RSC_EVENTS REDCORE_GLOBAL(htEvents)
#define RSC_OBJECTS REDCORE_GLOBAL(htObjects)
#define RSC_DEBUG REDCORE_GLOBAL(DebugRedCore)

#endif