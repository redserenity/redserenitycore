<?php
ini_set("redcore.debug", "1");
define("REDCORE_EXT", "RedCore");

$ExtLoaded = extension_loaded(REDCORE_EXT);
echo REDCORE_EXT . " extension " . ($ExtLoaded ? "is" : "is NOT") . " loaded!" . PHP_EOL;
if (!$ExtLoaded) exit();

echo "Available Functions: " . PHP_EOL;
print_r(get_extension_funcs(REDCORE_EXT));

echo ConfirmRedCoreCompiled('RedCore') . PHP_EOL;

echo "Interfaces: " . PHP_EOL;
foreach (get_declared_interfaces() as $Interface) {
    if (substr_count($Interface, "RedSerenity") > 0) {
        echo "  $Interface" . PHP_EOL;
    }
}
echo PHP_EOL;
echo "Classes: " . PHP_EOL;
foreach (get_declared_classes() as $Class) {
    if (substr_count($Class, "RedSerenity") > 0) {
        echo "  $Class" . PHP_EOL;
    }
}
echo PHP_EOL;

/*//(new \RedSerenity\Core\Events())->__EVENT__();
\RedSerenity\Core\Events::__EVENT__("Test", ['Hello', 'World']);

exit;



class MyOnEvents implements \RedSerenity\Core\OnEvents {
    public function __TesT() {
        echo " { __Test() Worked! }\n";
    }

    public function __BeforeEvent($OriginalClass=NULL, string $Method=NULL, array $Arguments=NULL) {
        echo "__BeforeEvent [" . get_class($OriginalClass) . "::$Method]\n";
    }

    public function __AfterEvent($OriginalClass=NULL, string $Method=NULL, $ReturnValue=NULL) {
        echo "__AfterEvent [" . get_class($OriginalClass) . "::$Method]\n";
    }
}

class MyTest {
    public function __construct(string $Test) { echo "$Test" . PHP_EOL; var_dump(\RedSerenity\Core\Events::RegisteredEvents()); }
    static public function Hello() { echo "Hello, Static!" . PHP_EOL; return TRUE; }
}

\RedSerenity\Core\Events::RegisterEvent(MyTest::class, MyOnEvents::class);
var_dump(\RedSerenity\Core\Events::RegisteredEvents());
//\RedSerenity\Core\Events::UnregisterEvent(MyTest::class, MyOnEvents::class);
//var_dump(\RedSerenity\Core\Events::RegisteredEvents());
new MyTest("Hello, UOPZ!");
MyTest::Hello();
exit;
*/

class nStdClass implements \RedSerenity\Core\Service {}

class WithCons implements \RedSerenity\Core\Service {

    public function __construct() {
        echo "WithCons->construct()" . PHP_EOL;
    }

}

class SharedCons extends \RedSerenity\Core\SharedService {

}

/*class BreakIt implements \RedSerenity\Core\Injectable {

    public function __inject(nStdClass $class, WithCons $_withCons, SharedCons $_shared) {}

}*/

class Test implements \RedSerenity\Core\Injectable {

	public $isTest = true;

    public function __construct() {
        echo static::class . "::__construct()" . PHP_EOL;
    }

	public function __inject(nStdClass $class, WithCons $_withCons, SharedCons $_shared) {}
	static public function __injectStatic(nStdClass $s_class, WithCons $s_withCons, SharedCons $s_shared) {}
}

class TestTwo extends Test {

	public function __inject(nStdClass $du, WithCons $_withCons) {}
	static public function __injectStatic() {}
}

echo "Class Test implements these interfaces:" . PHP_EOL;
print_r(class_implements(Test::class));



$R = new ReflectionClass('Test');
var_dump($R->getMethods());

echo "Setting \$S->HeyThere = 'Big Boy!'\n";
$S = SharedCons::__Instance();
$S->HeyThere = "Big Boy!";

echo "Instantiating Test()\n";
$T = new Test();
var_dump($T);
echo "Instantiating TestTwo()\n";
$TT = new TestTwo();
var_dump($TT);
var_dump($TT->_shared->HeyThere);
//throw new RedSerenity\Core\Exception("Test");

$SS = SharedCons::__Instance();
$S->HeyThere = "Pretty Lady!";
var_dump($T);
exit;




/*
    Garbage clean-up issue. Zend cleans up the first Singleton, and then tries to cleanup the second instance.
    But the object has already been cleaned up and does not exist, hence the error.

    Since there is a custom create method, there needs to be a custom destroy method to account for this.

    Does object exist? Yes, clean up. No, return SUCCESS.
*/

/*
class EventManagerService implements \RedSerenity\Core\Service {

    static public $EventManager;

    public function __construct() {
        if (!static::$EventManager) {
            static::$EventManager = new \RedSerenity\Core\Events\Manager();
            var_dump(static::$EventManager);
        }
    }

    public function __call(string $Method, Array $Arguments) {
        return call_user_func_array(array(&static::$EventManager, $Method),$Arguments);
    }

}

class MyEvent implements \RedSerenity\Core\Events\Event {

    public $Data;

    public function __construct(string $ArbitraryData) {
        $this->Data = $ArbitraryData;
    }

}

class MyWatcher implements \RedSerenity\Core\Events\Watcher {

    const PRIORITY = \RedSerenity\Core\Events\Type::PRIORITY_NORMAL;

    public function __onEvent(\RedSerenity\Core\Events\Event $Event) {
        echo $Event->Data;
    }

}

class MyCoolClass implements \RedSerenity\Core\Injectable {

    public function __inject(EventManagerService $EventManager) {}

    public function __construct() {

        $this->EventManager->Fire(new MyEvent("Hello, World!"));

    }

}
*/

//$EMS = new \RedSerenity\Core\Events\Manager();
/*$EMS->Fire();
$EMS->Register();
$EMS->Unregister();
*/
//print_r($EMS->Listeners());

//\RedSerenity\Core\Events\Manager::Watch(MyWatcher::class, MyEvent::class);

//$T = new MyCoolClass();