#include "dummy.h"
#include "../Helpers/helpers.h"

PHP_FUNCTION(ConfirmRedCoreCompiled) {
	char *arg = NULL;
	size_t arg_len, len;
	zend_string *strg;

	if (zend_parse_parameters(ZEND_NUM_ARGS(), "s", &arg, &arg_len) == FAILURE) {
		return;
	}

	strg = strpprintf(0, "Congratulations! Module %.78s is now compiled into PHP.", "RedCore", arg);

	RETURN_STR(strg);
}

PHP_FUNCTION(TestHelpers) {

/*	zend_object *stdClass;
	ZendClassCreate(zend_standard_class_def, stdClass);

	RETVAL_OBJ(stdClass);*/

}