//
// Created by Tyler Andersen on 10/23/17.
//

#ifndef REDCORE_DUMMY_H
#define REDCORE_DUMMY_H

#include "redcore.h"

PHP_FUNCTION(ConfirmRedCoreCompiled);
PHP_FUNCTION(TestHelpers);

static const zend_function_entry RedCoreFunctions[] = {
	PHP_FE(ConfirmRedCoreCompiled, NULL)
	PHP_FE(TestHelpers, NULL)
	PHP_FE_END
};

#endif //REDCORE_DUMMY_H
