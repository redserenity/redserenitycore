#include <zend_compile.h>
#include "events.h"
#include "execute.h"
#include "handlers.h"
#include "hooks.h"

zend_class_entry *RsCore_EventClass_ce;
zend_class_entry *RsCore_OnEventInterface_ce;

void ParseMethodExecuteData(zend_execute_data *ExecuteData, zend_class_entry *Scope, zval *This, zend_string *ClassName, zend_string *MethodName) {
	This = ((Z_TYPE((ExecuteData)->This) == IS_OBJECT) ? &(ExecuteData)->This : NULL);
	Scope = zend_get_called_scope(ExecuteData);
	MethodName = ExecuteData->func->common.function_name;
	ClassName = Scope->name;
}


ZEND_METHOD(Events, __EVENT__) {
	/* Get Method & Class Data */
	zval *This = NULL;
	zend_class_entry *Scope = NULL;
	zend_string *ClassName = NULL, *MethodName = NULL;

	This = ((Z_TYPE((execute_data)->This) == IS_OBJECT) ? &(execute_data)->This : NULL);
	Scope = zend_get_called_scope(execute_data);
	MethodName = execute_data->func->common.function_name;
	ClassName = Scope->name;

	/* Get Method Paramaters */
	zval *MethodParams = NULL;
	int NumMethodParams = 0;
	if (zend_parse_parameters(ZEND_NUM_ARGS(), "*", &MethodParams, &NumMethodParams) == FAILURE) {
		php_error(E_ERROR, "Could not get method parameters from %s::%s", ZSTR_VAL(ClassName), ZSTR_VAL(MethodName));
		return;
	}

	/* Debug Only */
	for (int i = 0; i < NumMethodParams; i++) {
		php_printf("Params Type: %i\n", Z_TYPE(MethodParams[i]));
	}
	//call_user_function(EventFunctionTable, )
	//call_user_function(FunctionTable, Scope, ThisFunction, RetVal, num_args, params);






	/* Debug Only */
	char *CalledType;
	if (!This) { CalledType = "::"; } else { CalledType = "->"; }
	php_printf("%s%s%s\n", ZSTR_VAL(ClassName), CalledType, ZSTR_VAL(MethodName));
}















/* Events::RegisterEvent(string $TheClass, string $EventClass) */
PHP_METHOD(Events, RegisterEvent) {
	zend_string *OriginalClass, *EventClass;
	HashTable *Hooks;
	zend_object *EventObject;
	zval HookObject;

	if (zend_parse_parameters_ex(ZEND_PARSE_PARAMS_QUIET, ZEND_NUM_ARGS(), "SS", &OriginalClass, &EventClass) != SUCCESS) {
		EventException("Invalid parameters");
		RETURN_BOOL(0);
	}

	zend_class_entry *EventCe = zend_lookup_class(EventClass);
	if (!EventCe) {
		EventException("Could not find class %s.", ZSTR_VAL(EventClass));
		RETURN_BOOL(0);
	}

	if (!instanceof_function(EventCe, RsCore_OnEventInterface_ce)) {
		EventException("Class %s must implement interface %s.", ZSTR_VAL(EventCe->name), ZSTR_VAL(RsCore_OnEventInterface_ce->name));
		RETURN_BOOL(0);
	}

	if (instanceof_function(zend_lookup_class(OriginalClass), RsCore_OnEventInterface_ce)) {
		EventException("You cannot add events to %s because it implements %s and this would create a circular reference problem.", ZSTR_VAL(OriginalClass), ZSTR_VAL(RsCore_OnEventInterface_ce->name));
		RETURN_BOOL(0);
	}

	zend_class_entry *Ce = zend_lookup_class(OriginalClass);
	if (Ce) {
		php_printf(":: Original Class %s\n", ZSTR_VAL(OriginalClass));
		php_printf("  :: Number of class methods %i\n", Ce->function_table.nNumOfElements);
		HashTable *ClassFunctions = &Ce->function_table;

		zend_ulong Index;
		zend_string *Key;
		zval *Value;
		ZEND_HASH_FOREACH_KEY_VAL(ClassFunctions, Index, Key, Value) {
			zend_function *Method = Z_FUNC_P(Value);
			php_printf("    :: Function: %s\n", ZSTR_VAL(Method->common.function_name));
		} ZEND_HASH_FOREACH_END();

	} else {
		php_printf(":: No Original class???\n");
	}

	Hooks = zend_hash_find_ptr(&RSC_EVENTS, OriginalClass);
	if (!Hooks) {
		ALLOC_HASHTABLE(Hooks);
		zend_hash_init(Hooks, 8, NULL, ZVAL_PTR_DTOR, 0);
	}

	if (!zend_hash_find_ptr(Hooks, EventCe->name)) {
		EventObject = zend_objects_new(EventCe);
		object_properties_init(EventObject, EventCe);
		ZVAL_OBJ(&HookObject, EventObject);

		if (!zend_hash_add_new(Hooks, EventCe->name, &HookObject)) {
			zval_ptr_dtor(&HookObject);
			RETURN_BOOL(0);
		}

		zval Arr;
		ZVAL_ARR(&Arr, Hooks);
		if (!zend_hash_update(&RSC_EVENTS, OriginalClass, &Arr)) {
			zval_ptr_dtor(&HookObject);
			zval_ptr_dtor(&Arr);
			RETURN_BOOL(0);
		}
	}
	HashTable *Test = &RedCore_globals.htEvents;
	RETURN_BOOL(1);
}

/* Events::UnregisterEvent(string $TheClass, string $EventClass) */
PHP_METHOD(Events, UnregisterEvent) {
	zend_string *OriginalClass, *EventClass;
	HashTable *Hooks;

	if (zend_parse_parameters_ex(ZEND_PARSE_PARAMS_QUIET, ZEND_NUM_ARGS(), "SS", &OriginalClass, &EventClass) != SUCCESS) {
		EventException("Invalid parameters");
	}

	Hooks = zend_hash_find_ptr(&RSC_EVENTS, OriginalClass);
	if (!Hooks) {
		RETURN_BOOL(1); //Should we throw an warning of class not found or what?
	}

	if (zend_hash_exists(Hooks, EventClass)) {
		zend_hash_del(Hooks, EventClass);
		RETURN_BOOL(1);
	}

	RETURN_BOOL(0);
}

/* Events::RegisteredEvents() */
PHP_METHOD(Events, RegisteredEvents) {
	zval *HashElement;
	zend_string *IndexString;
	zend_ulong IndexNumber;

	/* How can we return the HashTable directly? */
	array_init(return_value);
	ZEND_HASH_FOREACH_KEY_VAL(&RSC_EVENTS, IndexNumber, IndexString, HashElement) {
		add_assoc_zval(return_value, ZSTR_VAL(IndexString), HashElement);
	} ZEND_HASH_FOREACH_END();
}


void MINIT_EventHooks() {
	OverloadExecutors();
	OverloadOpcodeHandlers();

	zend_class_entry ceInitEventClass;
	INIT_NS_CLASS_ENTRY(ceInitEventClass, NAMESPACE_CORE, "Events",
											RsCore_EventClass_Functions)
	RsCore_EventClass_ce = zend_register_internal_class(&ceInitEventClass);

	zend_class_entry ceInitOnEventInterface;
	INIT_NS_CLASS_ENTRY(ceInitOnEventInterface, NAMESPACE_CORE, "OnEvents",
											RsCore_OnEventInterface_Functions)
	RsCore_OnEventInterface_ce = zend_register_internal_interface(&ceInitOnEventInterface);
}

void MSHUTDOWN_EventHooks() {
	RestoreOpcodeHandlers();
	RestoreExecutors();
}