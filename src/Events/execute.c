#include "execute.h"

typedef void (*zend_execute_internal_f) (zend_execute_data *, zval *);
typedef void (*zend_execute_f) (zend_execute_data *);

/*void red_events_execute_internal(zend_execute_data *execute_data, zval *return_value);
void red_events_execute(zend_execute_data *execute_data);*/

zend_execute_internal_f original_zend_execute_internal;
zend_execute_f original_zend_execute_ex;

void red_events_execute_internal(zend_execute_data *execute_data, zval *return_value) {
	if (original_zend_execute_internal) {
		original_zend_execute_internal(execute_data, return_value);
	} else execute_internal(execute_data, return_value);
}

void red_events_execute(zend_execute_data *execute_data) {
	if (original_zend_execute_ex) {
		original_zend_execute_ex(execute_data);
	} else execute_ex(execute_data);
}


void OverloadExecutors() {
	/*original_zend_execute_internal = zend_execute_internal;
	zend_execute_internal = red_events_execute_internal;
	original_zend_execute_ex = zend_execute_ex;
	zend_execute_ex = red_events_execute;*/
}

void RestoreExecutors() {
	/*zend_execute_internal = original_zend_execute_internal;
	zend_execute_ex = original_zend_execute_ex;*/
}