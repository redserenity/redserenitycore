#ifndef REDCORE_EXECUTE_H
#define REDCORE_EXECUTE_H

#include "redcore.h"

void OverloadExecutors();
void RestoreExecutors();

#endif //REDCORE_EXECUTE_H
