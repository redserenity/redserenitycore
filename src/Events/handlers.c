#include <zend_compile.h>
#include "handlers.h"
#include "redcore.h"
#include "events.h"

#define RE_SET_HANDLER(o) (ORIGINAL_##o) = zend_get_user_opcode_handler((o)); zend_set_user_opcode_handler((o), (REDEVENTS_##o));
#define RE_UNSET_HANDLER(o) zend_set_user_opcode_handler(o, ORIGINAL_##o);
#define RE_HANDLER(o) int REDEVENTS_##o(zend_execute_data *execute_data)
#define RE_CASE_CALL_ORIGINAL(o) case o: if (ORIGINAL_##o) return ORIGINAL_##o(execute_data);

user_opcode_handler_t ORIGINAL_ZEND_DO_FCALL;
user_opcode_handler_t ORIGINAL_ZEND_INIT_FCALL;
user_opcode_handler_t ORIGINAL_ZEND_INIT_FCALL_BY_NAME;
user_opcode_handler_t ORIGINAL_ZEND_INIT_NS_FCALL_BY_NAME;
user_opcode_handler_t ORIGINAL_ZEND_INIT_METHOD_CALL;
user_opcode_handler_t ORIGINAL_ZEND_INIT_STATIC_METHOD_CALL;

user_opcode_handler_t ORIGINAL_ZEND_EXT_FCALL_BEGIN;
user_opcode_handler_t ORIGINAL_ZEND_EXT_FCALL_END;

void ExecuteHook(char *EventHookName, zend_object *Object, zend_string *FunctionName) {
	zend_fcall_info Fci;
	zend_fcall_info_cache Fcc;
	zval EventParams[3], RetVal, zObj, zFunctionName, zNothing;

	/*zend_class_entry *old_scope = EG(fake_scope);
	EG(fake_scope) = Object->ce;*/

	zend_string *HookName = zend_string_init(EventHookName, strlen(EventHookName), 0);
	zend_function *EventFunction = Object->handlers->get_method(&Object, HookName, NULL);
	zend_string_release(HookName);

	if (EventFunction) {
		ZVAL_NULL(&zNothing);
		ZVAL_OBJ(&zObj, Object);
		ZVAL_STR(&zFunctionName, FunctionName);
		/*red_call_method(&zObj, Object->ce, NULL, EventHookName, strlen(EventHookName), &RetVal, 3, &zObj, &zFunctionName, &zNothing);*/

		ZVAL_COPY_VALUE(&EventParams[0], &zNothing);
		ZVAL_COPY_VALUE(&EventParams[1], &zFunctionName);
		ZVAL_COPY_VALUE(&EventParams[2], &zNothing);

		Fci.size = sizeof(Fci);
		Fci.no_separation = 1;
		Fci.object = Object;
		Fci.retval = &RetVal;
		Fci.param_count = 3;
		Fci.params = EventParams;
		ZVAL_UNDEF(&Fci.function_name);

		Fcc.initialized = 1;
		Fcc.function_handler = EventFunction;
		Fcc.calling_scope = Object->ce;
		Fcc.called_scope = Object->ce;
		Fcc.object = Object;

		int CallSuccess = zend_call_function(&Fci, &Fcc);
		/*zval_ptr_dtor(&EventParams[0]);
		zval_ptr_dtor(&EventParams[1]);d
		zval_ptr_dtor(&EventParams[2]);*/
		//zval_ptr_dtor(&RetVal);
		/*if (CallSuccess == FAILURE) {
			php_error_docref(NULL, E_WARNING, "Could not call %s::%s()", ZSTR_VAL(Object->ce->name), ZSTR_VAL(FunctionName));
			//EG(fake_scope) = old_scope;
			return;
		}*/

		if (!Z_ISUNDEF(RetVal)) {
			zval_ptr_dtor(&RetVal);
		}
	}

	//EG(fake_scope) = old_scope;
}

void ExecuteOriginalCall(zend_execute_data *Call) {
	zend_fcall_info Fci;
	zend_fcall_info_cache Fcc;

	zend_function *Func = Call->func;
	zend_class_entry *Scope = Func->common.scope;
	zend_object *Object = Z_OBJ(Call->This);
	zval RetVal;

	Fci.size = sizeof(Fci);
	Fci.no_separation = 1;
	Fci.object = Object;
	Fci.retval = &RetVal;
	Fci.param_count = ZEND_CALL_NUM_ARGS(Call);
	Fci.params = ZEND_CALL_ARG(Call, 1);
	ZVAL_UNDEF(&Fci.function_name);

	Fcc.initialized = 1;
	Fcc.function_handler = Func;
	Fcc.calling_scope = Object->ce;
	Fcc.called_scope = Object->ce;
	Fcc.object = Object;

	int CallSuccess = zend_call_function(&Fci, &Fcc);
	if (CallSuccess == FAILURE) {
		php_error_docref(NULL, E_WARNING, "Could not call %s::%s()", ZSTR_VAL(Object->ce->name), ZSTR_VAL(Func->common.function_name));
		return;
	}

	if (!Z_ISUNDEF(RetVal)) {
		zval_ptr_dtor(&RetVal);
	}
}

void ProcessHooks(zend_execute_data *Call) {
	zend_function *Func = Call->func;
	zend_class_entry *Scope = Func->common.scope;
	zend_object *Object = Z_OBJ(Call->This);
	HashTable *Hooks;
	HashTable *Test = &RedCore_globals.htEvents;

	if (!zend_hash_exists(&RSC_EVENTS, Scope->name)) {
		return;
	}

	Hooks = zend_hash_find_ptr(&RSC_EVENTS, Scope->name);
	if (!Hooks) { return; } // Throw error here?

	zval *Hook;
	ZEND_HASH_FOREACH_VAL(Hooks, Hook) {
		if (Hook) {
			if (Z_TYPE_P(Hook) == IS_OBJECT) {
				php_printf("  Hook is %s->__BeforeEvent\n", ZSTR_VAL(Z_OBJ_P(Hook)->ce->name));
				ExecuteHook(BEFORE_EVENT, Z_OBJ_P(Hook), Func->common.function_name);
				if (!Object->ce) {
					php_printf("  Calling %s::%s\n", ZSTR_VAL(Scope->name), ZSTR_VAL(Func->common.function_name));
				} else {
					php_printf("  Calling %s->%s\n", ZSTR_VAL(Object->ce->name), ZSTR_VAL(Func->common.function_name));
				}
				ExecuteOriginalCall(Call);
				php_printf("  Hook is %s->__AfterEvent\n", ZSTR_VAL(Z_OBJ_P(Hook)->ce->name));
				ExecuteHook(AFTER_EVENT, Z_OBJ_P(Hook), Func->common.function_name);
			}

		}
	} ZEND_HASH_FOREACH_END();

}

bool IsHookRegistered(zend_execute_data *Call) {
	zend_function *Func = Call->func;
	zend_class_entry *Scope = Func->common.scope;

	if (zend_hash_exists(&RSC_EVENTS, Scope->name)) {
		return 1;
	}

	return 0;
}

RE_HANDLER(ZEND_DO_FCALL) {
	zend_execute_data *Call = EX(call);

	/* Is this OPCODE calling a Class Method? */
	if (Call) {
		zend_function *Func = Call->func;
		zend_class_entry *Scope = Func->common.scope;

		if (Scope) { // Yes it is!
			/* We are not going to let you create an event for an OnEvent instance. Circular loop and all... */
			if (instanceof_function(Scope, RsCore_OnEventInterface_ce)) {
				return ZEND_USER_OPCODE_DISPATCH;
			}

			zval This = Call->This;
			zend_string *ClassName = Scope->name;

			/*if (ZVAL_IS_NULL(&This)) {
				php_printf("Going to call %s::%s\n", ZSTR_VAL(ClassName), ZSTR_VAL(Func->common.function_name));
			} else {
				php_printf("Going to call %s->%s\n", ZSTR_VAL(ClassName), ZSTR_VAL(Func->common.function_name));
			}*/

			HashTable *Test = &RedCore_globals.htEvents;

			/*
			 * Is there a hook class registered for it?
			 * If yes, call the BEFORE hook, execute the originally called method,
			 *   call the AFTER hook, and then skip the original OPCODE called.
			 *   (Otherwise the originally called method will execute twice!)
			 */
			if (IsHookRegistered(Call)) {
				ProcessHooks(Call);
				/* Skip calling the original method so we don't execute twice! */
				EX(call) = Call->prev_execute_data;
				EX(opline) = EX(opline) + 1;
				return ZEND_USER_OPCODE_LEAVE;
				//return ZEND_USER_OPCODE_DISPATCH; // Temp during testing.
			}
		}
	}

	return ZEND_USER_OPCODE_DISPATCH;
}

RE_HANDLER(_ZEND_CALL_HANDLER) {
	zend_uchar OpCode = EX(opline)->opcode;

	switch (OpCode) {
		default: break;
	}

	switch (OpCode) {
		RE_CASE_CALL_ORIGINAL(ZEND_INIT_FCALL)
		RE_CASE_CALL_ORIGINAL(ZEND_INIT_FCALL_BY_NAME)
		RE_CASE_CALL_ORIGINAL(ZEND_INIT_NS_FCALL_BY_NAME)
		RE_CASE_CALL_ORIGINAL(ZEND_INIT_METHOD_CALL)
		RE_CASE_CALL_ORIGINAL(ZEND_INIT_STATIC_METHOD_CALL)
	}
	return ZEND_USER_OPCODE_DISPATCH;
}

RE_HANDLER(ZEND_INIT_FCALL) { return REDEVENTS__ZEND_CALL_HANDLER(execute_data); }
RE_HANDLER(ZEND_INIT_FCALL_BY_NAME) {	return REDEVENTS__ZEND_CALL_HANDLER(execute_data); }
RE_HANDLER(ZEND_INIT_NS_FCALL_BY_NAME) { return REDEVENTS__ZEND_CALL_HANDLER(execute_data); }
RE_HANDLER(ZEND_INIT_METHOD_CALL) { return REDEVENTS__ZEND_CALL_HANDLER(execute_data); }
RE_HANDLER(ZEND_INIT_STATIC_METHOD_CALL) { return REDEVENTS__ZEND_CALL_HANDLER(execute_data); }

void OverloadOpcodeHandlers() {
	RE_SET_HANDLER(ZEND_DO_FCALL)
	RE_SET_HANDLER(ZEND_INIT_FCALL_BY_NAME)
	RE_SET_HANDLER(ZEND_INIT_FCALL)
	RE_SET_HANDLER(ZEND_INIT_NS_FCALL_BY_NAME)
	RE_SET_HANDLER(ZEND_INIT_METHOD_CALL)
	RE_SET_HANDLER(ZEND_INIT_STATIC_METHOD_CALL)
}

void RestoreOpcodeHandlers() {
	RE_UNSET_HANDLER(ZEND_DO_FCALL)
	RE_UNSET_HANDLER(ZEND_INIT_FCALL)
	RE_UNSET_HANDLER(ZEND_INIT_FCALL_BY_NAME)
	RE_UNSET_HANDLER(ZEND_INIT_NS_FCALL_BY_NAME)
	RE_UNSET_HANDLER(ZEND_INIT_METHOD_CALL)
	RE_UNSET_HANDLER(ZEND_INIT_STATIC_METHOD_CALL)
}




ZEND_API zval* red_call_method(zval *object, zend_class_entry *obj_ce, zend_function **fn_proxy, const char *function_name, size_t function_name_len, zval *retval_ptr, int param_count, zval* arg1, zval* arg2, zval* arg3)  {
	int result;
	zend_fcall_info fci;
	zval retval;
	zval params[3];

	if (param_count > 0) {
		ZVAL_COPY_VALUE(&params[0], arg1);
	}
	if (param_count > 1) {
		ZVAL_COPY_VALUE(&params[1], arg2);
	}
	if (param_count > 2) {
		ZVAL_COPY_VALUE(&params[2], arg3);
	}

	fci.size = sizeof(fci);
	fci.object = object ? Z_OBJ_P(object) : NULL;
	fci.retval = retval_ptr ? retval_ptr : &retval;
	fci.param_count = param_count;
	fci.params = params;
	fci.no_separation = 1;

	if (!fn_proxy && !obj_ce) {
		/* no interest in caching and no information already present that is
		 * needed later inside zend_call_function. */
		ZVAL_STRINGL(&fci.function_name, function_name, function_name_len);
		result = zend_call_function(&fci, NULL);
		zval_ptr_dtor(&fci.function_name);
	} else {
		zend_fcall_info_cache fcic;
		ZVAL_UNDEF(&fci.function_name); /* Unused */

		fcic.initialized = 1;
		if (!obj_ce) {
			obj_ce = object ? Z_OBJCE_P(object) : NULL;
		}
		if (!fn_proxy || !*fn_proxy) {
			HashTable *function_table = obj_ce ? &obj_ce->function_table : EG(function_table);
			fcic.function_handler = zend_hash_str_find_ptr(
				function_table, function_name, function_name_len);
			if (fcic.function_handler == NULL) {
				/* error at c-level */
				zend_error_noreturn(E_CORE_ERROR, "Couldn't find implementation for method %s%s%s", obj_ce ? ZSTR_VAL(obj_ce->name) : "", obj_ce ? "::" : "", function_name);
			}
			if (fn_proxy) {
				*fn_proxy = fcic.function_handler;
			}
		} else {
			fcic.function_handler = *fn_proxy;
		}

		fcic.calling_scope = obj_ce;
		if (object) {
			fcic.called_scope = Z_OBJCE_P(object);
		} else {
			zend_class_entry *called_scope = zend_get_called_scope(EG(current_execute_data));

			if (obj_ce &&
					(!called_scope ||
					 !instanceof_function(called_scope, obj_ce))) {
				fcic.called_scope = obj_ce;
			} else {
				fcic.called_scope = called_scope;
			}
		}
		fcic.object = object ? Z_OBJ_P(object) : NULL;
		result = zend_call_function(&fci, &fcic);
	}
	if (result == FAILURE) {
		/* error at c-level */
		if (!obj_ce) {
			obj_ce = object ? Z_OBJCE_P(object) : NULL;
		}
		if (!EG(exception)) {
			zend_error_noreturn(E_CORE_ERROR, "Couldn't execute method %s%s%s", obj_ce ? ZSTR_VAL(obj_ce->name) : "", obj_ce ? "::" : "", function_name);
		}
	}
	/* copy arguments back, they might be changed by references */
	if (param_count > 0 && Z_ISREF(params[0]) && !Z_ISREF_P(arg1)) {
		ZVAL_COPY_VALUE(arg1, &params[0]);
	}
	if (param_count > 1 && Z_ISREF(params[1]) && !Z_ISREF_P(arg2)) {
		ZVAL_COPY_VALUE(arg2, &params[1]);
	}
	if (!retval_ptr) {
		zval_ptr_dtor(&retval);
		return NULL;
	}
	return retval_ptr;
}