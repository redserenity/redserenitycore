#ifndef REDCORE_HOOKS_H
#define REDCORE_HOOKS_H

#include "redcore.h"

typedef struct _EventHook {
	zend_class_entry *orig_class;
	zend_class_entry *event_class;
	zend_bool busy;
} EventHook;

#endif //REDCORE_HOOKS_H
