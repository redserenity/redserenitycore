#include <zend_compile.h>
#include "redcore.h"
#include "events.h"

zend_class_entry *RsCore_Events_EventInterface_ce;
zend_class_entry *RsCore_Events_WatcherInterface_ce;
zend_class_entry *RsCore_Events_ManagerClass_ce;

PHP_METHOD(Manager, __construct) {
	zval Listeners;
	array_init(&Listeners);
	add_property_zval(getThis(), "Listeners", &Listeners);
	zval_ptr_dtor(&Listeners);
}

PHP_METHOD(Manager, Fire) {
	zval *Event;
	zend_object *EventClass;

	if (zend_parse_parameters(ZEND_NUM_ARGS() TSRMLS_CC, "O|", &Event, RsCore_Events_EventInterface_ce) == FAILURE) {
		return;
	}

	//EventClass = Z_OBJ_P(Event);

	/*
	 *	GetWatchers
	 *	Loop Through Watchers
	 *	If Watcher is for this EventType
	 *		Call $Watcher->__OnEvent($Event)
	 */

	//zval_ptr_dtor(Event); //GC error

}

PHP_METHOD(Manager, Register) {


}

PHP_METHOD(Manager, Listeners) {
	if (zend_parse_parameters_none() == FAILURE) {
		return;
	}

	zval rv;
	zend_string *PropertyName = zend_string_init("Listeners", sizeof("Listeners")-1, 0);
	ZVAL_COPY(return_value, zend_read_property_ex(RsCore_Events_ManagerClass_ce, getThis(), PropertyName, 0, &rv));
	zend_string_release(PropertyName);
}

static int RedCoreEventsImplementEvent(zend_class_entry *interface, zend_class_entry *class_type TSRMLS_DC) {
	return SUCCESS;
}

static int RedCoreEventsImplementWatcher(zend_class_entry *interface, zend_class_entry *class_type TSRMLS_DC) {
	return SUCCESS;
}

void MINIT_EventsClassInterface() {
	zend_class_entry ceInitEventsEventInterface;
	INIT_NS_CLASS_ENTRY(ceInitEventsEventInterface, NAMESPACE_EVENTS, "Event",
											RsCore_Events_EventInterface_Functions)
	RsCore_Events_EventInterface_ce = zend_register_internal_interface(&ceInitEventsEventInterface);
	RsCore_Events_EventInterface_ce->interface_gets_implemented = RedCoreEventsImplementEvent;

	zend_class_entry ceInitEventsWatcherInterface;
	INIT_NS_CLASS_ENTRY(ceInitEventsWatcherInterface, NAMESPACE_EVENTS, "Watcher",
											RsCore_Events_WatcherInterface_Functions)
	RsCore_Events_WatcherInterface_ce = zend_register_internal_interface(&ceInitEventsWatcherInterface);
	RsCore_Events_WatcherInterface_ce->interface_gets_implemented = RedCoreEventsImplementWatcher;

	zend_class_entry ceInitEventsManagerClass;
	INIT_NS_CLASS_ENTRY(ceInitEventsManagerClass, NAMESPACE_EVENTS, "Manager",
											RsCore_Events_ManagerClass_Functions)
	RsCore_Events_ManagerClass_ce = zend_register_internal_class(&ceInitEventsManagerClass);
	zend_declare_property_null(RsCore_Events_ManagerClass_ce, "Listeners", sizeof("Listeners")-1, ZEND_ACC_PROTECTED);
}