#include "base.h"
#include "redcore.h"
#include "zend_exceptions.h"

zend_class_entry *RsCore_ExceptionBase_ce;

void MINIT_ExceptionBase() {
	zend_class_entry ceInitExceptionBase;
	INIT_NS_CLASS_ENTRY(ceInitExceptionBase, NAMESPACE_ROOT, "Exception",
											RsCore_ExceptionBase_Functions)
	RsCore_ExceptionBase_ce = zend_register_internal_class_ex(&ceInitExceptionBase, zend_ce_exception);
}