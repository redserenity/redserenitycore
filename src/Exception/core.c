#include "core.h"
#include "redcore.h"
#include "base.h"

zend_class_entry *RsCore_ExceptionCore_ce;

void MINIT_ExceptionCore() {
	zend_class_entry ceInitExceptionCore;
	INIT_NS_CLASS_ENTRY(ceInitExceptionCore, NAMESPACE_CORE, "Exception",
											RsCore_ExceptionCore_Functions)
	RsCore_ExceptionCore_ce = zend_register_internal_class_ex(&ceInitExceptionCore, RsCore_ExceptionBase_ce);
}