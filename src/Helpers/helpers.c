#include "helpers.h"

int ZendClassGetMethod(zend_class_entry *ce, zend_string *name, zend_function **function) {
	zend_string *key = zend_string_tolower(name);
	zend_function *ptr = zend_hash_find_ptr(&ce->function_table, key);
	zend_string_release(key);

	if (!ptr) { return FAILURE; }
	if (function) { *function = ptr; }

	return SUCCESS;
}



int ZendClassCreate(zend_class_entry *Ce, zend_object *Object) {
	Object = zend_objects_new(Ce);
	object_properties_init(Object, Ce);
	return SUCCESS;
}





typedef struct __Class {
	int (*New)(zend_object *Object, zval *Class);
	int (*Create)(zend_class_entry *Ce, zend_object *Object);
	int (*GetMethod)(zend_class_entry *Ce, zend_string *MethodName, zend_function **Function);
	int (*CallMethod)(zend_class_entry *Ce, zend_string *MethodName, zval *ReturnValue);
} _Class;

_Class Class() {
	_Class ClassHelper;

	ClassHelper.GetMethod = ZendClassGetMethod;


	return ClassHelper;
}
