#include <zend_interfaces.h>
#include "php_redcore.h"
#include "redcore.h"
#include "injectable.h"
#include "../service/service.h"

#define INJECT "__inject"
#define INJECT_STATIC "__injectStatic"

zend_class_entry *RsCore_InjectableInterface_ce;

static void CallConstructor(zend_object *Object) {
	zend_function *Constructor;

	Constructor = Object->handlers->get_constructor(Object);
	if (Constructor) {
		if (RSC_DEBUG) {
			php_printf("!\t\t(Invoking %s->__construct())\n", ZSTR_VAL(Object->ce->name));
		}

		zval ReturnValue;
		zval *Parameters = NULL;

		zend_fcall_info Fci;
		zend_fcall_info_cache Fcc;

		Fci.size = sizeof(Fci);
		ZVAL_UNDEF(&Fci.function_name);
		Fci.object = Object;
		Fci.retval = &ReturnValue;
		Fci.param_count = 0;
		Fci.params = Parameters;
		Fci.no_separation = 1;

		Fcc.initialized = 1;
		Fcc.function_handler = Constructor;
		Fcc.calling_scope = zend_get_executed_scope();
		Fcc.called_scope = Object->ce;
		Fcc.object = Object;

		if (EG(exception)) {
			zend_error(E_ERROR,
								 "Can't call %s->__construct() because there is a pending exception",
								 ZSTR_VAL(Object->ce->name)
			);
		}

		if (zend_call_function(&Fci, &Fcc) == FAILURE) {
			zend_error(E_WARNING, "Invocation of %s->__construct() failed", ZSTR_VAL(Object->ce->name));
		}
	}
}

static void SetProperty(zend_class_entry *Class, zend_object *Object, zend_string *PropertyName, zend_string *ClassName) {
	zend_class_entry *ArgCe;
	zend_object *ArgObj;
	zval PropertyValue;

	ArgCe = zend_lookup_class(ClassName);
	if (!ArgCe) {
		zend_error(E_ERROR,
							 "Could not lookup class %s specified in %s%s%s()",
							 ZSTR_VAL(ClassName),
							 (Object ? ZSTR_VAL(Object->ce->name) : ZSTR_VAL(Class->name)),
							 (Object ? "->" : "::"),
							 (Object ? INJECT : INJECT_STATIC)
		);
	}

	if (RSC_DEBUG) {
		php_printf(
			"!\tSetting %s%s%s = %s\n",
			(Object ? ZSTR_VAL(Object->ce->name) : ZSTR_VAL(Class->name)),
			(Object ? "->" : "::$"),
			ZSTR_VAL(PropertyName),
			ZSTR_VAL(ArgCe->name)
		);
	}

	if (!instanceof_function(ArgCe, RsCore_ServiceInterface_ce)) {
		zend_error(E_ERROR,
							 "Class %s does not implement %s\\Service. Only classes that implement %s\\Service can be used in %s().",
							 ZSTR_VAL(ArgCe->name),
							 NAMESPACE_CORE,
							 NAMESPACE_CORE,
							 (Object ? INJECT : INJECT_STATIC)
		);
	}

	if (instanceof_function(ArgCe, RsCore_SharedServiceClass_ce)) {
		if(!zend_call_method(NULL, ArgCe, NULL, "__instance", sizeof("__instance")-1, &PropertyValue, 0, NULL, NULL)) {
			zend_error(E_ERROR,
								 "Could not call %s::__instance() while trying to inject service.",
								 ZSTR_VAL(ArgCe->name)
			);
		}
	} else {
		ArgObj = zend_objects_new(ArgCe);
		if (!ArgObj) {
			zend_error(E_ERROR,
								 "Could not instantiate class %s specified in %s%s%s()",
								 ZSTR_VAL(ArgCe->name),
								 (Object ? ZSTR_VAL(Object->ce->name) : ZSTR_VAL(Class->name)),
								 (Object ? "->" : "::"),
								 (Object ? INJECT : INJECT_STATIC)
			);
		}

		object_properties_init(ArgObj, ArgCe);
		CallConstructor(ArgObj);
		ZVAL_OBJ(&PropertyValue, ArgObj);
	}

	if (Object) {
		zval ZObject;
		Z_OBJ(ZObject) = Object;
		zend_update_property_ex(Object->ce, &ZObject, PropertyName, &PropertyValue);
	} else {
		zend_declare_property_null(Class, ZSTR_VAL(PropertyName), ZSTR_LEN(PropertyName), ZEND_ACC_PUBLIC|ZEND_ACC_STATIC);
		zend_update_static_property(Class, ZSTR_VAL(PropertyName), ZSTR_LEN(PropertyName), &PropertyValue);
	}

	if (RSC_DEBUG) {
		php_printf("!\t\tSuccess!\n");
	}

	zval_ptr_dtor(&PropertyValue);
}

static zend_function *GetFunction(zend_class_entry *Class, zend_object *Object, char *FunctionName) {
	zend_function *ReturnValue;
	zend_string *_FunctionName;

	_FunctionName = zend_string_init(FunctionName, strlen(FunctionName), 0);
	if (Object) {
		ReturnValue = Object->handlers->get_method(&Object, _FunctionName, NULL);
		if (!ReturnValue) {
			zend_string_release(_FunctionName);
			zend_error(E_ERROR, "%s\\Injectable could not get %s->%s.", NAMESPACE_CORE, ZSTR_VAL(Object->ce->name), FunctionName);
		}
	} else {
		if (Class->get_static_method) {
			ReturnValue = Class->get_static_method(Class, _FunctionName);
		} else {
			ReturnValue = zend_std_get_static_method(Class, _FunctionName, NULL);
		}
		if (!ReturnValue) {
			zend_string_release(_FunctionName);
			zend_error(E_ERROR, "%s\\Injectable could not get %s::%s.", NAMESPACE_CORE, ZSTR_VAL(Class->name), FunctionName);
		}
	}

	zend_string_release(_FunctionName);
	return ReturnValue;
}

static void ProcessInjectableArguments(zend_class_entry *Class, zend_object *Object, zend_class_entry *ParentClass, zend_object *ParentObject) {
	zend_function *InjectFunction;
	zend_arg_info *Arguments;
	uint32_t i, ArgumentsCount;

	if (Object) {
		if (ParentObject) {
			InjectFunction = GetFunction(NULL, ParentObject, INJECT);
		} else {
			InjectFunction = GetFunction(NULL, Object, INJECT);
		}

		if (RSC_DEBUG) {
			if (ParentObject) {
				php_printf("!\tProcessing %s->__inject()\n", ZSTR_VAL(ParentObject->ce->name));
			} else {
				php_printf("!\tProcessing %s->__inject()\n", ZSTR_VAL(Object->ce->name));
			}
		}
	} else {
		if (ParentClass) {
			InjectFunction = GetFunction(ParentClass, NULL, INJECT_STATIC);
		} else {
			InjectFunction = GetFunction(Class, NULL, INJECT_STATIC);
		}


		if (RSC_DEBUG) {
			if (ParentClass) {
				php_printf("!\tProcessing %s::__injectStatic()\n", ZSTR_VAL(ParentClass->name));
			} else {
				php_printf("!\tProcessing %s::__injectStatic()\n", ZSTR_VAL(Class->name));
			}
		}
	}

	Arguments = InjectFunction->common.arg_info;
	ArgumentsCount = InjectFunction->common.num_args;

	for (i = 0; i < ArgumentsCount; i++) {
		if (!Arguments->class_name) {
			zend_error(E_ERROR,
								 "Scalar TypeHints are not allowed for %s() method in class %s. $%s as a(n) %s is not allowed.",
								 (Object ? INJECT : INJECT_STATIC),
								 ZSTR_VAL(Class->name),
								 ZSTR_VAL(Arguments->name),
								 zend_get_type_by_const(Arguments->type_hint)
			);
		}

		if (
			zend_binary_strcasecmp(ZSTR_VAL(Arguments->class_name), ZSTR_LEN(Arguments->class_name), "self", sizeof("self")- 1) == 0 ||
			zend_binary_strcasecmp(ZSTR_VAL(Arguments->class_name), ZSTR_LEN(Arguments->class_name), "parent", sizeof("parent")- 1) == 0
		) {
			zend_error(E_ERROR,
								 "%s%s%s() (or it's parent) uses 'self' or 'parent' which is not allowed. It would create a loop.",
								 (Object ? ZSTR_VAL(Object->ce->name) : ZSTR_VAL(Class->name)),
								 (Object ? "->" : "::"),
								 (Object ? INJECT : INJECT_STATIC)
			);
		}

		/* TODO: Check to see if property already exists on Object or Class.
		 * If it does, that means that we are a parent class trying to overwrite
		 * what is already there. Should we allow it or disallow it? */
		if (Object) {
			SetProperty(NULL, Object, Arguments->name, Arguments->class_name);
		} else {
			SetProperty(Class, NULL, Arguments->name, Arguments->class_name);
		}

		Arguments++;
	}
}

static void ProcessInjectableClass(zend_class_entry *Class, zend_object *Object) {
	zend_class_entry *ParentClass;
	zend_object *ParentObject;

	if (Object) {
		ProcessInjectableArguments(NULL, Object, NULL, NULL);
	} else {
		ProcessInjectableArguments(Class, NULL, NULL, NULL);
	}

	if (Class->parent) {
		ParentClass = Class->parent;

		while (ParentClass) {
			if (RSC_DEBUG) {
				php_printf("!\t%s has parent %s\n", ZSTR_VAL(Class->name), ZSTR_VAL(ParentClass->name));
			}

			if (Object) {
				/* I don't think this is working right...
				 * Don't really think we should be creating the parent class as an object.
				 * We should probably just loop through the ParentClass but stick the
				 * properties on Object */
				ParentObject = zend_objects_new(ParentClass);
				ProcessInjectableArguments(NULL, Object, NULL, ParentObject);
			} else {
				ProcessInjectableArguments(Class, NULL, ParentClass, NULL);
			}

			ParentClass = ParentClass->parent;
		}
	}
}

static zend_object *InjectionCreateObject(zend_class_entry *Class) {
	zend_object *ReturnValue;
	ReturnValue = zend_objects_new(Class);
	object_properties_init(ReturnValue, Class);
	ProcessInjectableClass(Class, ReturnValue);
	return ReturnValue;
}

static int RedCoreImplementInjectable(zend_class_entry *Interface, zend_class_entry *Class TSRMLS_DC) {
	if (Class->create_object != NULL) {
		zend_error(E_ERROR, "%s interface can only be used on userland classes (or internal classes without a custom create_object function).", ZSTR_VAL(Interface->name));
	}

	if (RSC_DEBUG) {
		php_printf("!\t%s implements %s\n", ZSTR_VAL(Class->name), ZSTR_VAL(Interface->name));
	}

	Class->create_object = InjectionCreateObject;
	ProcessInjectableClass(Class, NULL);

	return SUCCESS;
}

void MINIT_InjectableInterface() {
	zend_class_entry ceInitInjectableInterface;
	INIT_NS_CLASS_ENTRY(ceInitInjectableInterface, NAMESPACE_CORE, "Injectable",
											RsCore_InjectableInterface_Functions)
	RsCore_InjectableInterface_ce = zend_register_internal_interface(&ceInitInjectableInterface);
	RsCore_InjectableInterface_ce->interface_gets_implemented = RedCoreImplementInjectable;
}
