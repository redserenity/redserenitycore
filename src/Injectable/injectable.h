#ifndef REDCORE_INJECTABLE_H
#define REDCORE_INJECTABLE_H

#include "redcore.h"

void MINIT_InjectableInterface();
static int RedCoreImplementInjectable(zend_class_entry *interface, zend_class_entry *class_type);

static const zend_function_entry RsCore_InjectableInterface_Functions[] = {
	ZEND_ABSTRACT_ME(Injectable, __inject, NULL)
	ZEND_FENTRY(__injectStatic, NULL, NULL, ZEND_ACC_PUBLIC|ZEND_ACC_ABSTRACT|ZEND_ACC_STATIC)
	ZEND_FE_END
};

extern zend_class_entry *RsCore_InjectableInterface_ce;

#endif //REDCORE_INJECTABLE_H
