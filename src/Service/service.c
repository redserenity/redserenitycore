#include <zend_compile.h>
#include "redcore.h"
#include "service.h"

static void CallConstructor(zend_object *Object) {
	zend_function *Constructor;

	Constructor = Object->handlers->get_constructor(Object);
	if (Constructor) {
		zval retval;
		zval *params = NULL;
		int ret, i;
		zend_fcall_info fci;
		zend_fcall_info_cache fcc;

		fci.size = sizeof(fci);
		ZVAL_UNDEF(&fci.function_name);
		fci.object = Object;
		fci.retval = &retval;
		fci.param_count = 0;
		fci.params = params;
		fci.no_separation = 1;

		fcc.initialized = 1;
		fcc.function_handler = Constructor;
		fcc.calling_scope = zend_get_executed_scope();
		fcc.called_scope = Object->ce;
		fcc.object = Object;

		ret = zend_call_function(&fci, &fcc);
		if (ret == FAILURE) {
			php_error_docref(NULL, E_WARNING, "Invocation of %s's constructor failed", ZSTR_VAL(Object->ce->name));
		}

	}
}

zend_class_entry *RsCore_ServiceInterface_ce;
zend_class_entry *RsCore_SharedServiceClass_ce;

PHP_METHOD(SharedService, __construct) {}
PHP_METHOD(SharedService, __clone) {}
PHP_METHOD(SharedService, __wakeup) {}

PHP_METHOD(SharedService, __instance) {
	zend_class_entry *called_scope = zend_get_called_scope(execute_data);
	if (!called_scope) {
		zend_error(E_ERROR, "Could not determine what my class is from \\RedSerenity\\Core\\SharedServices::__instance().");
	}

	zval *InstancePtr;
	InstancePtr = zend_read_static_property(called_scope, "__INSTANCE__", sizeof("__INSTANCE__")-1, 1);

	if (InstancePtr && Z_TYPE_P(InstancePtr) == IS_OBJECT) {
		RETURN_ZVAL(InstancePtr, 1, 0);
	}

	zval Instance;
	zend_object *Object = zend_objects_new(called_scope);
	ZVAL_OBJ(&Instance, Object);
	object_properties_init(Object, called_scope);
	zend_update_static_property(called_scope, "__INSTANCE__", sizeof("__INSTANCE__")-1, &Instance);

	CallConstructor(Object);

	RETVAL_ZVAL(&Instance, 1, 0);
	zval_ptr_dtor(&Instance);
}

static int RedCoreImplementService(zend_class_entry *interface, zend_class_entry *class_type TSRMLS_DC) {
	if (class_type->constructor) {
		if (class_type->constructor->common.num_args > 0) {
			zend_error_noreturn(E_ERROR, "Classes that implement %s cannot have constructors with arguments. Remove arguments from %s::__construct().", ZSTR_VAL(interface->name), ZSTR_VAL(class_type->name));
			return FAILURE;
		}
	}

	return SUCCESS;
}

void MINIT_ServiceClassInterface() {
	zend_class_entry ceInitServiceInterface;
	INIT_NS_CLASS_ENTRY(ceInitServiceInterface, NAMESPACE_CORE, "Service",
											RsCore_ServiceInterface_Functions)
	RsCore_ServiceInterface_ce = zend_register_internal_interface(&ceInitServiceInterface);
	RsCore_ServiceInterface_ce->interface_gets_implemented = RedCoreImplementService;

	zend_class_entry ceInitSharedServiceClass;
	INIT_NS_CLASS_ENTRY(ceInitSharedServiceClass, NAMESPACE_CORE, "SharedService",
											RsCore_SharedServiceClass_Functions)
	RsCore_SharedServiceClass_ce = zend_register_internal_class(&ceInitSharedServiceClass);
	zend_class_implements(RsCore_SharedServiceClass_ce TSRMLS_CC, 1, RsCore_ServiceInterface_ce);
	zend_declare_property_null(RsCore_SharedServiceClass_ce, "__INSTANCE__", sizeof("__INSTANCE__")-1, ZEND_ACC_PROTECTED|ZEND_ACC_STATIC);
}