#include "singleton.h"

zend_class_entry *RsCore_SingletonInterface_ce;
zend_object_handlers RsCore_SingletonInterface_handlers;

static void SingletonObjectsDtor(zend_object *Object) {
	if (Object->gc.refcount > 1) {
		Object->gc.refcount--;
	} else {
		zend_objects_destroy_object(Object);
	}
}

static void SingletonObjectsFree(zend_object *Object) {
	if (Object->gc.refcount > 1) {
		Object->gc.refcount--;
	} else {
		zend_object_std_dtor(Object);
	}
}

static zend_object *SingletonCreateClass(zend_class_entry *ce) {
	zend_object *Object;
	zval *Instance;
	Instance = zend_read_static_property(ce, "__Instance", sizeof("__Instance")-1, 1);

	if (Z_TYPE_P(Instance) == IS_OBJECT) {
		Object = Z_OBJ_P(Instance);
		Object->gc.refcount++;
		return Object;
	}

	Object = zend_objects_new(ce);
	ZVAL_OBJ(Instance, Object);
	object_properties_init(Object, ce);
	Object->handlers = &RsCore_SingletonInterface_handlers;
	zend_update_static_property(ce, "__Instance", sizeof("__Instance")-1, Instance);
	return Object;
}

static int RedCoreImplementSingleton(zend_class_entry *interface, zend_class_entry *class_type TSRMLS_DC) {
	zend_error(E_ERROR, "%s interface does not work yet... stay tuned...", ZSTR_VAL(interface->name));
	if (class_type->create_object != NULL) {
		zend_error(E_ERROR, "%s interface can only be used on userland classes.", ZSTR_VAL(interface->name));
	}

	class_type->create_object = SingletonCreateClass;
	zend_declare_property_null(class_type, "__Instance", sizeof("__Instance")-1, ZEND_ACC_PRIVATE|ZEND_ACC_STATIC);

	return SUCCESS;
}

void MINIT_SingletonInterface() {
	zend_class_entry ceInitSingletonInterface;
	INIT_NS_CLASS_ENTRY(ceInitSingletonInterface, NAMESPACE_CORE, "Singleton",
											RsCore_SingletonInterface_Functions)
	RsCore_SingletonInterface_ce = zend_register_internal_interface(&ceInitSingletonInterface);
	RsCore_SingletonInterface_ce->interface_gets_implemented = RedCoreImplementSingleton;


	memcpy(&RsCore_SingletonInterface_handlers, zend_get_std_object_handlers(), sizeof RsCore_SingletonInterface_handlers);
	RsCore_SingletonInterface_handlers.dtor_obj = SingletonObjectsDtor;
	RsCore_SingletonInterface_handlers.free_obj = SingletonObjectsFree;
}