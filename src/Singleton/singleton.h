#ifndef REDCORE_SINGLETON_H
#define REDCORE_SINGLETON_H

#include "redcore.h"

extern zend_class_entry *RsCore_SingletonInterface_ce;

void MINIT_SingletonInterface();

static const zend_function_entry RsCore_SingletonInterface_Functions[] = {
	ZEND_FE_END
};

#endif //REDCORE_SINGLETON_H
